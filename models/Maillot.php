<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maillot".
 *
 * @property string $código
 * @property string $tipo
 * @property string $color
 * @property int $premio
 */
class Maillot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maillot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['código', 'tipo', 'color', 'premio'], 'required'],
            [['premio'], 'integer'],
            [['código'], 'string', 'max' => 3],
            [['tipo'], 'string', 'max' => 30],
            [['color'], 'string', 'max' => 20],
            [['código'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'código' => 'Código',
            'tipo' => 'Tipo',
            'color' => 'Color',
            'premio' => 'Premio',
        ];
    }
}
